Rails.application.routes.draw do
  resources :resumes
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  get 'resumes/downloadFile/:id', to:'resumes#downloadFile', as: 'download_file'
end
